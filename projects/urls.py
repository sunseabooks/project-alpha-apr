from django.urls import path
from .views import projectsList, projectDetail, projectCreate

urlpatterns = [
    path("", projectsList, name="list_projects"),
    path("<int:pk>/", projectDetail, name="show_project"),
    path("create/", projectCreate, name="create_project"),
]
