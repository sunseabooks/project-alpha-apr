from django.shortcuts import render, redirect
from .models import Project
from .forms import ProjectForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required


@login_required
def projectsList(request):
    projects = Project.objects.filter(members=request.user)
    context = {
        "projects": projects,
    }
    return render(request, "projects/list.html", context)


@login_required
def projectDetail(request, pk):
    project = Project.objects.get(pk=pk)
    tasks = Task.objects.filter(project=pk)
    context = {
        "project": project,
        "tasks": tasks,
    }
    return render(request, "projects/detail.html", context)


@login_required
def projectCreate(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.save()
            form.save_m2m()
            return redirect("show_project", project.id)
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create.html", context)
