from django.urls import path
from .views import taskCreate, tasksList, taskUpdate

urlpatterns = [
    path("create/", taskCreate, name="create_task"),
    path("mine/", tasksList, name="show_my_tasks"),
    path("<int:pk>/complete/", taskUpdate, name="complete_task"),
]
