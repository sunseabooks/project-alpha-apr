from django.shortcuts import render, redirect
from .models import Task
from .forms import TaskForm
from django.contrib.auth.decorators import login_required


@login_required
def taskCreate(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        id = request.POST["project"]
        if form.is_valid():
            task = form.save(commit=False)
            task.save()
            form.save_m2m
            return redirect("show_project", id)
    else:
        form = TaskForm()
    context = {"form": form}
    return render(request, "tasks/create.html", context)


@login_required
def tasksList(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks}
    return render(request, "tasks/list.html", context)


@login_required
def taskUpdate(request, pk):
    if request.method == "POST":
        old = Task.objects.get(pk=pk)
        old.is_completed = request.POST.get("is_completed")
        old.save()
        return redirect("show_my_tasks")
